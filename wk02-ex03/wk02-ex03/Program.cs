﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk02_ex03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Would you like to convert miles or kilometers?");

            var metric = Console.ReadLine();
            const double mile2km = 1.609344;
            const double km2mile = 0.621371;
            switch (metric)
            {
            case "miles":
            Console.WriteLine("How many miles would you like converted?");
                var getMiles = double.Parse(Console.ReadLine());
                var roundingMiles = System.Math.Round(getMiles * mile2km, 2);
                Console.WriteLine($"{getMiles} miles is {roundingMiles} kilometers");
                break;

            case "kilometers":
            Console.WriteLine("How many kilometers would you like converted?");
                var getKM = double.Parse(Console.ReadLine());
                var roundingKM = System.Math.Round(getKM * km2mile, 2);
                Console.WriteLine($"{getKM} kilometers is {roundingKM} miles");
                break;

            default:
            Console.WriteLine("Please type kilometers or miles");
            break;
            }
        } 
    
    } 
}
